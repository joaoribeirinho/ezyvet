<div class="menu_holder">
    <div class="container">
        <div class="navbar-header">
            <a href="products">
                <img src="images/ezyVet_logo.png" id="logoimg" />
            </a>
        </div>

        <button class="menu-toggler">
            <div id="nav-icon1">
                <span></span>
                <span></span>
                <span></span>
            </div>
        </button>

        <div class="menu">
            <ul class="menu-nav">
                <li class="dropdown" id="m_home">
                    <a href="products">Products</a>
                </li>

                <?
                if(Prods::total_items() > 0){
                    $cart_count = "(" . Prods::total_items() . ")";
                }else{
                    $cart_count = "";
                }
                ?>

                <li class="dropdown" id="m_cart">
                    <a href="cart">Cart <?= $cart_count?></a>
                </li>
            </ul>
        </div>
    </div>
</div>