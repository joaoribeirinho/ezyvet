<?
Class Prods{

    public static function products() {
        return [
            [ "name" => "Sledgehammer", "price" => 125.75 ],
            [ "name" => "Axe", "price" => 190.50 ],
            [ "name" => "Bandsaw", "price" => 562.131 ],
            [ "name" => "Chisel", "price" => 12.9 ],
            [ "name" => "Hacksaw", "price" => 18.45 ],
        ];
    }

    public static function prices() {
        $prices = array();
        foreach(Prods::products() as $product){
            $name = $product["name"];
            $price = $product["price"];
            $prices[$name] = $price;
        }
        return $prices;
    }

    public static function add_to_cart($pn) {

        if(empty($_SESSION["cart"]) || !isset($_SESSION["cart"])){

            $new_prod = [
                ["name" => $pn, "qt" => 1]
            ];
            $cart = $new_prod;

        }else{
            $cart = $_SESSION["cart"];
            foreach ($cart as $index => $item){
                if($item["name"] == $pn){
                    $cart[$index]["qt"] = $item["qt"] + 1;
                    $incremented = true;
                }
            }

            if(!$incremented){
                $new_prod = array(
                    "name" => $pn,
                    "qt" => 1
                );
                array_push($cart, $new_prod);
            }
        }

        $_SESSION["cart"] = $cart;

        return $_SESSION["cart"];
    }

    public static function remove_from_cart($index) {
        unset($_SESSION["cart"][$index]);

        return $_SESSION["cart"];
    }

    public static function total_items() {
        $total_items = 0;
        $cart = $_SESSION["cart"];
        foreach ($cart as $index => $item){
            $total_items = $total_items + $item["qt"];
        }

        return $total_items;
    }
}
?>
