<head>
    <meta charset="utf-8" />
    <title>ezyVet</title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport" />

    <!-- Chrome, Firefox OS and Opera -->
    <meta name="theme-color" content="#f8f9fa">

    <!-- CSS -->
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
    <link href="js/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>

    <!-- FAVICON -->
    <link rel="shortcut icon" href="https://www.ezyvet.com/wp-content/uploads/2015/09/favicon.ico.gzip" type="image/x-icon">

    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300&display=swap" rel="stylesheet">

    <link href="css/custom.css" rel="stylesheet" type="text/css"/>
    <script src="js/plugins/jquery-1.10.2.min.js" type="text/javascript"></script>

</head>