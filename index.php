<?
error_reporting(E_STRICT);
session_start();
include 'functions.php';

$products = Prods::products();


if(!empty($_GET["pn"])){
    $pn = urldecode($_GET["pn"]);
    Prods::add_to_cart($pn);

    ?>
    <script>window.location.replace('products?add=true');</script>
    <?
}elseif($_GET["add"]){
    $added = true;
}
?>

<!DOCTYPE html>
<html>
    <?
    include 'header.php';
    ?>

    <body>

      <?
      include 'menu.php';
      ?>

      <?
      if($added){
          ?>
          <div class="msg_added">Item added to cart</div>
          <?
      }
      ?>

      <div class="bloco">
          <div class="container">

              <?
              foreach($products as $product){
                  $price = sprintf("%.2f", $product["price"]);
                  ?>
                  <div class="col-md-3">
                      <div class="each_product">
                          <div>
                              <img src="images/ezyvet/<?= $product["name"]?>.jpg" />
                          </div>
                          <div class="name"><?= $product["name"]?></div>
                          <div class="price">£<?= $price?></div>
                          <a href="products?pn=<?= urlencode($product["name"])?>">
                              <div class="add"><i class="fa fa-plus" aria-hidden="true"></i> Add to cart</div>
                          </a>
                      </div>
                  </div>
                  <?
              }
              ?>
          </div>
      </div>


      <?
      include 'javascripts.php';
      ?>
    </body>
</html>



