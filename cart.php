<?
error_reporting(E_STRICT);
session_start();
include 'functions.php';

$products = Prods::products();
$prices = Prods::prices();

if(isset($_GET["remove"])){
    $removed = true;
    $remove_index = $_GET["remove"];

    Prods::remove_from_cart($remove_index);
}
?>

<!DOCTYPE html>
<html>
  <?
  include 'header.php';
  ?>

  <body>
    <?
    include 'menu.php';
    ?>

    <?
    if($removed){
        ?>
        <div class="msg_removed">Item removed from cart</div>
        <?
    }
    ?>

    <div class="bloco">
        <div class="container">
            <div class="col-md-12">

                <h1>Cart</h1>

                <table class="cart table table-striped">
                    <thead>
                        <th>Img</th>
                        <th>Product Name</th>
                        <th>Price</th>
                        <th>Quantity</th>
                        <th>Total</th>
                        <th>Remove</th>
                    </thead>

                    <tbody>
                        <?
                        $cart = $_SESSION["cart"];
                        $cart_total = 0.0;
                        foreach($cart as $index => $item){
                            $name = $item["name"];
                            $price = $prices[$name];
                            $price = sprintf("%.2f", $price);

                            $total_price = $item["qt"] * $price;
                            $total_price = sprintf("%.2f", $total_price);

                            $cart_total = $cart_total + $total_price;
                            ?>
                            <tr>
                                <td>
                                    <img src="images/ezyvet/<?= $name?>.jpg" />
                                </td>

                                <td><?= $name?></td>
                                <td>£<?= $price?></td>
                                <td><?= $item["qt"]?></td>
                                <td>£<?= $total_price?></td>
                                <td>
                                    <a href="cart?remove=<?= $index?>">
                                        <i class="fa fa-trash" aria-hidden="true"></i>
                                    </a>
                                </td>
                            </tr>
                            <?
                        }
                        ?>
                    </tbody>
                </table>

                <div class="total">
                    <span>Total: </span>
                    <span class="price">£<?= sprintf("%.2f", $cart_total)?></span>
                </div>

            </div>
        </div>
    </div>

    <?
    include 'javascripts.php';
    ?>
  </body>
</html>



